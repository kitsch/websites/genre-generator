import GenreGenerator from './lib/GenreGenerator';
import GenreTween from './lib/GenreTween';

const genreEl = document.getElementById("genre");

const generator = new GenreGenerator();
const genreTween = new GenreTween((phase) => {
    genreEl.style.fontSize = `${.9 + phase * .25}em`;
});

function generate () {
    genreEl.innerText = generator.generate();
    genreTween.start();
}
generate();

document.getElementById("regenerate").onclick = generate;