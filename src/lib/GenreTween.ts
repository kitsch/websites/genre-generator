export default class GenreTween {
    protected phase = 1;
    protected static DELTA = 1 / 60;
    protected static SPEED = 4;

    constructor (protected callback: (phase: number) => any) {
        window.setInterval(this.update, GenreTween.DELTA * 1000);
    }

    protected doCallback () {
        this.callback(Math.sin(this.phase * Math.PI));
    }

    public start () {
        this.phase = 0;
        this.doCallback();
    }

    protected update = () => {
        if (this.phase === 1) return;

        this.phase = Math.min(1, this.phase + GenreTween.DELTA * GenreTween.SPEED);
        this.doCallback();
    }
}