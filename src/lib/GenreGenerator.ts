import Data from "./data";

const modes = [
    "NOUN+NOUN",
    "NOUN+ROOT",
    "NOUN+SUFFIX",
    "PREFIX+ROOT",
    "ROOT+ROOT",
    "ROOT+SUFFIX"
];

export default class GenreGenerator {
    public generate () {
        const id = Math.floor(Math.random() * modes.length);
        switch (modes[id]) {
            case "NOUN+NOUN":
                const noun1 = this.getRandomNoun();
                const noun2 = this.getRandomNoun();
                if (noun1 === noun2) return this.generate();
                return `${noun1} ${noun2}`;

            case "NOUN+ROOT":
                return `${this.getRandomNoun()} ${this.getRandomRoot()}`;
                
            case "NOUN+SUFFIX":
                return `${this.getRandomNoun()}${this.getRandomSuffix()}`;
    
            case "PREFIX+ROOT":
                return `${this.getRandomPrefix()}${this.getRandomRoot()}`;
                    
            case "ROOT+ROOT":
                    const root1 = this.getRandomRoot();
                    const root2 = this.getRandomRoot();
                    if (root1 === root2) return this.generate();
                return `${root1} ${root2}`;
    
            case "ROOT+SUFFIX":
                return `${this.getRandomRoot()}${this.getRandomSuffix()}`;
        }
        return "";
    }

    protected getRandom (array: string[]) {
        return array[Math.floor(array.length * Math.random())];
    }
    protected getRandomNoun () {
        return this.getRandom(Data.nouns);
    }
    protected getRandomPrefix () {
        return this.getRandom(Data.prefixes);
    }
    protected getRandomRoot () {
        return this.getRandom(Data.roots);
    }
    protected getRandomSuffix () {
        return this.getRandom(Data.suffixes);
    }
}